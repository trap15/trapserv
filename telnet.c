/*****************************************************************************
 *  TrapNET Server                                                           *
 *  Copyright (C)2009-2010    trap15 (Alex Marshall) <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2                                            *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <errno.h>
#include "types.h"

#include "connection.h"
#include "telnet.h"

void telnet_log(const char *fmt, ...)
{
	char *buf = NULL;
	unsigned int sz = sizeof(struct sockaddr_in);
	va_list va;
	va_start(va, fmt);
	vasprintf(&buf, fmt, va);
	va_end(va);
	if(sock_a.sin_port == 0) {
		getpeername(sock, (struct sockaddr *)&sock_a, &sz);
		sock_a.sin_addr.s_addr = ntohl(sock_a.sin_addr.s_addr);
	}
	printf("[%d.%d.%d.%d] %s",	(sock_a.sin_addr.s_addr >> 24) & 0xFF, \
					(sock_a.sin_addr.s_addr >> 16) & 0xFF, \
					(sock_a.sin_addr.s_addr >>  8) & 0xFF, \
					(sock_a.sin_addr.s_addr >>  0) & 0xFF, \
					 buf);
	free(buf);
}

void send_command(telnet_command cmd, u8* args, int argcnt)
{
	char* out;
	int i;
	out = calloc(argcnt + 3, 1);
	out[0] = 0;	/* The padding helps it get through */
	out[1] = 0xFF;
	out[2] = cmd;
	for(i = 0; i < argcnt; i++)
		out[3 + i] = args[i];
	switch(cmd) {
		case TELNET_WILL:
			telnet_log("WILL(%d) Sent\n", args[0]);
			break;
		case TELNET_WONT:
			telnet_log("WONT(%d) Sent\n", args[0]);
			break;
		case TELNET_DO:
			telnet_log("DO  (%d) Sent\n", args[0]);
			break;
		case TELNET_DONT:
			telnet_log("DONT(%d) Sent\n", args[0]);
			break;
		case TELNET_SB:
			telnet_log("SB  (%d) Sent\n", args[0]);
			break;
	}
	write_data(out, argcnt + 3);
}

void telnet_abilities()
{
	u8 o = OPTION_SUPPRESS_GA;
	send_command(TELNET_WILL, &o, 1);
	o = OPTION_ECHO;
	send_command(TELNET_WILL, &o, 1);
}

static int cannot_handles(u8 op)
{
	int handled = 1;
	switch(op) {
		case OPTION_STATUS:
			send_command(TELNET_WONT, &op, 1);
			break;
		case OPTION_FLOW_CONTROL:
			send_command(TELNET_WONT, &op, 1);
			break;
		case OPTION_AUTH:
			send_command(TELNET_WONT, &op, 1);
			break;
		case OPTION_ENCRYPT_OPT:
			send_command(TELNET_WONT, &op, 1);
			break;
		case OPTION_TIMING_MARK:
			send_command(TELNET_WONT, &op, 1);
			break;
		default:
			handled = 0;
			break;
	}
	if(!handled)
		return 0;
	return 1;
}	

static void handle_sb(u8 op, u8 *params, int count)
{
	
}

static void handle_will(u8 op)
{
	u8 params[64];
	cannot_handles(op);
	switch(op) {
		case OPTION_STATUS:
			send_command(TELNET_DONT, &op, 1);
			break;
		case OPTION_TERM_TYPE:
			send_command(TELNET_DONT, &op, 1);
			break;
		case OPTION_NEGO_WINDOW_SIZE:
			send_command(TELNET_DONT, &op, 1);
			break;
		case OPTION_TERM_SPEED:
			send_command(TELNET_DONT, &op, 1);
			break;
		case OPTION_FLOW_CONTROL:
			send_command(TELNET_DONT, &op, 1);
			break;
		case OPTION_LINEMODE:
			send_command(TELNET_DO, &op, 1);
			params[0] = op;
			params[1] = 1;
			params[2] = 2;
			params[3] = 0xFF;
			params[4] = TELNET_SE;
			send_command(TELNET_SB, params, 5);
			break;
		case OPTION_X_DISP_LOC:
			send_command(TELNET_DONT, &op, 1);
			break;
		case OPTION_NEW_ENV:
			send_command(TELNET_DONT, &op, 1);
			break;
	}
}

static void handle_wont(u8 op)
{
	switch(op) {
		case 0:
			break;
	}
}

static void handle_do(u8 op)
{
	cannot_handles(op);
	switch(op) {
		case OPTION_SUPPRESS_GA:
			send_command(TELNET_WILL, &op, 1);
			break;
	}
}

static void handle_dont(u8 op)
{
	switch(op) {
		case 0:
			break;
	}
}

void do_telnet_command()
{
	u8 params[64];
	u8 cmd = 0;
	int i = 0;
	u8 c = 0;
	int ret = 1;
	int firstrun = 1;
	int done = 0;
	bzero(params, 64);
	while(!done) {
		if(firstrun)
			c = 0xFF;
		else
			ret = read(sock, &c, 1);
		firstrun = 0;
		if(ret == 1) {
			if((cmd == 0) && (c == 0xFF)) {
				while(!cmd) {
					ret = read(sock, &c, 1);
					if((ret == 1) && (c == 0xFF))
						continue;
					else if(ret == 1) {
						cmd = c;
					}else if(errno == EINTR)
						continue;
					else
						break;
				}
				continue;
			}
			if(cmd == 0)
				break;
			params[i++] = c;
			switch(cmd) {
				case TELNET_IP:
					telnet_log("IP received\n");
					kill_connection = 2;
					return;
				case TELNET_AYT:
					telnet_log("Yes I'm here!\n");
					send_command(TELNET_NOP, NULL, 0);
					return;
				case TELNET_SB:
					if((i > 1) && (params[i - 2] == 0xFF) && (params[i - 1] == TELNET_SE)) {
						telnet_log("SB  (%d)\n", params[0]);
						handle_sb(params[0], params + 1, i - 1);
						return;
					}
					break;
				case TELNET_WILL:
					telnet_log("WILL(%d)\n", c);
					handle_will(c);
					return;
				case TELNET_WONT:
					telnet_log("WONT(%d)\n", c);
					handle_wont(c);
					return;
				case TELNET_DO:
					telnet_log("DO  (%d)\n", c);
					handle_do(c);
					return;
				case TELNET_DONT:
					telnet_log("DONT(%d)\n", c);
					handle_dont(c);
					return;
				default:
					return;
			}
		}else if(ret == 0) {
			return;
		}else{
			if(errno == EINTR)
				continue;
			return;
		}
	}
	return;
}
