
OBJECTS		 = main.o connection.o shell.o telnet.o iniparser/dictionary.o iniparser/iniparser.o
OUTPUT		 = trapserv
DEFS		 = 
OPTIMIZE	 = -O3
LIBS		 = 
LIBDIRS		 = 
INCLUDES	 = -I$(BASE)/include -I$(BASE)/hyperscam/include
CFLAGS		 = -g -std=c89 -Wall -pedantic $(INCLUDES) $(OPTIMIZE)
LDFLAGS		 = $(LIBDIRS) $(LIBS)
CC		 = gcc
RM		 = rm

all: $(OUTPUT)
%.o: %.m
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<
%.o: %.c
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	$(CC) $(LDFLAGS) -o $(OUTPUT) $(OBJECTS)
clean:
	$(RM) -f $(OUTPUT) $(OBJECTS)
run:
	./$(OUTPUT)
release: clean


