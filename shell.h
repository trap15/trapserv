/*****************************************************************************
 *  TrapNET Server                                                           *
 *  Copyright (C)2009-2010    trap15 (Alex Marshall) <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2                                            *
 *****************************************************************************/

#ifndef SHELL_H_
#define SHELL_H_

#define PERM_EXIST		0x00000001
#define PERM_SHUTDOWN		0x40000000

#define CMP_U32(x, y)	(((u32)(x)) == ((u32)(y)))

#define READLINE(buf)					\
buf = read_line();					\
if(buf == NULL) {					\
	write_line("\nError reading stream.\n");	\
	telnet_log("Error reading stream.\n");		\
	return 1;					\
}else if(CMP_U32(buf, 0xFFFFFFFF)) {			\
	write_line("\nCtrl-C\n");			\
	telnet_log("Ctrl-C\n");				\
	return 1;					\
}else if(CMP_U32(buf, 0xFFFFFFFE)) {			\
	return 1;					\
}else if(CMP_U32(buf, 0xFFFFFFFD)) {			\
	write_line("\nServer is going down.\n");	\
	return 1;					\
}

typedef struct {
	char*	user_db;
	u16	port;
	int	max_cons;
	int	max_logins;
	int	dmon;
	char*	motd;
	char*	uid_db;
	u32	default_gid;
	u32	default_perms;
} config_t;

typedef struct {
	int	exist;
	u64	pass;
	u32	uid;
	u32	gid;
	u32	perms;
} user_t;

extern user_t user;
extern config_t config;
extern int error_level;

int initialize_shell();
int destroy_shell();
int shell(char *buf);
int shell_login();
int shell_logout();
u64 encrypt_pass(char *pass);
u64 uchecksum(void *rbuf, int len);

#endif /* SHELL_H_ */
