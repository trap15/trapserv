/*****************************************************************************
 *  TrapNET Server                                                           *
 *  Copyright (C)2009-2010    trap15 (Alex Marshall) <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2                                            *
 *****************************************************************************/

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <netdb.h>
#include "types.h"

#include "connection.h"
#include "telnet.h"

#define KILL_PATH_CORE	"kill_squidnet"
#define KILL_PATH	killfil

static int error = 0;
static int escmode = 0;
int kill_connection = 0;
int *kill_serv = NULL;
int sock = -1;
int stop_echo = 0;
int include_crlf = 0;
struct sockaddr_in sock_a;
char coredir[256];
char killfil[256];

static ssize_t write_line_core(const char *fmt, ...)
{
	ssize_t len;
	char *buf = NULL;
	va_list va;
	va_start(va, fmt);
	vasprintf(&buf, fmt, va);
	va_end(va);
	
	len = write_data(buf, strlen(buf));
	free(buf);
	return len;
}

static void disp_char(char c, char* buf, ssize_t *n, int ech)
{
	if(escmode == 1) {
		if(c == 0x5B) {
			escmode++;
		}else{
			escmode = 0;
		}
		return;
	}
	if(escmode == 2) {
		if(c == 0x33) {
			escmode++;
		}else{
			escmode = 0;
		}
		return;
	}
	if(escmode == 3) {
		escmode = 0;
		return;
	}
	if((c >= 0x20) && (c < 0x7F)) {
		if(ech)
			write_line_core("%c", c);
		if((buf != NULL) && (n != NULL)) {
			buf[*n] = c;
			*n = (*n) + 1;
		}
	}else{
		switch(c) {
			case 0x7F:
			case 8:
				if((buf != NULL) && (n != NULL)) {
					if((*n) > 0) {
						if(ech)
							write_line_core("\x1B[D \x1B[D");
						buf[(*n) - 1] = 0;
						*n = (*n) - 1;
					}
				}
				break;
			case 9:
				if(ech)
					write_line_core("\t");
				if((buf != NULL) && (n != NULL)) {
					buf[*n] = c;
					*n = (*n) + 1;
				}
				break;
			case 0x1B:
				escmode = 1;
				break;
			case CR:
			case LF:
				if(ech)
					write_line_core("\r\n");
				if((buf != NULL) && (n != NULL)) {
					buf[*n] = '\n';
					*n = (*n) + 1;
				}
				break;
		}
	}
}

static void kill_zombies()
{
	while(waitpid(-1, NULL, WNOHANG) > 0);
}

void kill_sock()
{
	kill_connection = 3;
}

static void initialize_connection(int dmon)
{
	signal(SIGCHLD, kill_zombies);
	getcwd(coredir, 256);
	sprintf(KILL_PATH, "%s/%s", coredir, KILL_PATH_CORE);
	if(dmon)
		daemon(0, 0);
}

static int serve_port(u16 port, int max_connections)
{
	int s;
	int ret;
	struct sockaddr_in sa;
	
	bzero(&sa, sizeof(struct sockaddr_in));
	sa.sin_family		= AF_INET;
	sa.sin_port		= htons(port);
	sa.sin_addr.s_addr	= INADDR_ANY;
	s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(s < 0) {
		error = s;
		return -2;
	}
	ret = bind(s, (struct sockaddr *)&sa, sizeof(struct sockaddr_in));
	if(ret < 0) {
		error = ret;
		close(s);
		return -3;
	}
	listen(s, max_connections);
	fcntl(s, F_SETFL, O_NONBLOCK);
	return(s);
}

static int get_connection(int s)
{
	sock = accept(s, NULL, NULL);
	if(sock < 0)
		return -1;
	fcntl(sock, F_SETFL, 0);
	return sock;
}

void kill_server()
{
	char killbuf[] = "Kill";
	FILE* killfp = fopen(KILL_PATH, "wb");
	if(killfp) {
		fwrite(killbuf, 4, 1, killfp);
		fclose(killfp);
	}
	kill_connection = 1;
}

char* read_line()
{
	char *buf = calloc(2, 1);
	ssize_t n, ret;
	u8 c;
	n = 0;
	for(; buf != NULL;) {
		ret = read(sock, &c, 1);
		if(ret == 1) {
			if(kill_connection)
				return NULL;
			if(c == 0xFF) {
				do_telnet_command();
			}else if(c == 3) {
				kill_connection = 2;
				return NULL;
			}else{
				if(!include_crlf) {
					if((c == CR) || (c == LF)) {
						break;
					}
				}
				disp_char(c, buf, &n, !stop_echo);
				buf = realloc(buf, n + 2);
				if((c == CR) || (c == LF)) {
					break;
				}
			}
		}else if(ret == 0) {
			if(n == 1)
				return NULL;
			break;
		}else{
			if(errno == EINTR)
				continue;
			return NULL;
		}
	}
	
	buf = realloc(buf, n + 2);
	buf[n] = 0;
	return buf;
}

ssize_t write_data(const char *buf, ssize_t size)
{
	size_t nleft;
	ssize_t nwritten;
	
	nleft = size;
	
	while(nleft > 0) {
		nwritten = write(sock, buf, nleft);
		if(nwritten <= 0) {
			if(errno == EINTR)
				nwritten = 0;
			else
				return -1;
		}
		nleft -= nwritten;
		buf += nwritten;
	}
	return size;
}

ssize_t write_line(const char *fmt, ...)
{
	int i;
	int len;
	char *buf = NULL;
	va_list va;
	va_start(va, fmt);
	vasprintf(&buf, fmt, va);
	va_end(va);
	len = strlen(buf);
	for(i = 0; i < len; i++) {
		disp_char(buf[i], NULL, NULL, 1);
	}
	free(buf);
	return i;
}

int server_start(u16 port, int max_connections, int dmon, connection_handler_t conhandle)
{
	int p;
	int takedown = 0;
	int again;
	int forkret = 0;
	FILE* killfp;
	char killbuf[4];
	int pids[65536];
	int pidcnt = 0;
	bzero(pids, 65536 * sizeof(int));
	bzero(&sock_a, sizeof(struct sockaddr_in));
	printf("Starting server on port %d...\n", port);
	printf("Max connections: %d\n", max_connections);
	printf("Daemon: %s\n", dmon ? "YES" : "NO");
try_again:
	p = serve_port(port, max_connections);
	if(p < 0) {
		if(errno == EADDRINUSE) {
			printf("Address in use. Retrying in 10secs...\n");
			sleep(10);
			printf("Retrying...\n");
			goto try_again;
		}
		perror("Unable to establish connection");
		return 0;
	}
	printf("Port being served...\n");
	initialize_connection(dmon);
	printf("Now accepting connections!\n");
	while(!takedown) {
		again = 0;
		sock = get_connection(p);
		killfp = fopen(KILL_PATH, "rb");
		if(killfp) {
			fread(killbuf, 4, 1, killfp);
			fclose(killfp);
			if(memcmp(killbuf, "Kill", 4) == 0)
				takedown = 1;
		}
		if(sock < 0) {
			switch(errno) {
				case EINTR:
				case ENOENT:
				case ECHILD:
				case EAGAIN:
#if EAGAIN != EWOULDBLOCK
				case EWOULDBLOCK:
#endif
					again = 1;
				default:
					break;
			}
			if(again)
				continue;
			printf("errno: %d\n", errno);
			perror("accept() failed");
			return 0;
		}
		forkret = fork();
		switch(forkret) {
			case -1:
				perror("fork() failed");
				close(p);
				close(sock);
				return 0;
			case 0:
				close(p);
				telnet_log("Connected!\n");
				signal(SIGTERM, kill_sock);
				(conhandle)();
				telnet_log("Disconnecting!\n");
				shutdown(sock, SHUT_RDWR);
				close(sock);
				return 1;
			default:
				pids[pidcnt++] = forkret;
				close(sock);
				continue;
		}
	}
	printf("Server is going down!\n");
	shutdown(p, SHUT_RDWR);
	close(p);
	bzero(killbuf, 4);
	killfp = fopen(KILL_PATH, "wb");
	if(killfp) {
		fwrite(killbuf, 4, 1, killfp);
		fclose(killfp);
	}
	for(p = 0; p < pidcnt; p++) {
		kill(pids[p], SIGTERM);
	}
	return 1;
}
