/*****************************************************************************
 *  TrapNET Server                                                           *
 *  Copyright (C)2009-2010    trap15 (Alex Marshall) <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2                                            *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <errno.h>
#include "types.h"

#include "iniparser/iniparser.h"
#include "connection.h"
#include "telnet.h"
#include "shell.h"

#define CONF_PATH		"server.conf"
#define DEFAULT_USERDB		"userdb.conf"
#define DEFAULT_UIDDB		"uid.db"
#define DEFAULT_MOTD		"Welcome to TrapNET!\n"
#define DEFAULT_PORT		TELNET_PORT
#define DEFAULT_CONNECTS	20
#define DEFAULT_DMON		0
#define DEFAULT_LOGIN_TRIES	5
#define DEFAULT_GID		1
#define DEFAULT_PERMS		0x00000001

config_t config;

void conhandle();

int main(int argc, char *argv[])
{
	int ret = 1;
	printf("TrapNET Server (C)2009-2010 trap15 (Alex Marshall)\n");
	printf("Loading...\n");
	dictionary *ini = iniparser_load(CONF_PATH);
	if(ini == NULL) {
		fprintf(stderr, "Unable to load config file from %s. Using defaults...\n", CONF_PATH);
		config.port		= DEFAULT_PORT;
		config.max_cons		= DEFAULT_CONNECTS;
		config.max_logins	= DEFAULT_LOGIN_TRIES;
		config.dmon		= DEFAULT_DMON;
		config.motd		= strdup(DEFAULT_MOTD);
		config.uid_db		= strdup(DEFAULT_UIDDB);
		config.user_db		= strdup(DEFAULT_USERDB);
		config.default_gid	= DEFAULT_GID;
		config.default_perms	= DEFAULT_PERMS;
	}else{
		config.port		= iniparser_getint	(ini, "server:port", DEFAULT_PORT);
		config.max_cons		= iniparser_getint	(ini, "server:connections", DEFAULT_CONNECTS);
		config.max_logins	= iniparser_getint	(ini, "server:logintries", DEFAULT_LOGIN_TRIES);
		config.dmon		= iniparser_getboolean	(ini, "server:daemon", DEFAULT_DMON);
		config.motd		= iniparser_getstring	(ini, "server:motd", DEFAULT_MOTD);
		config.user_db		= iniparser_getstring	(ini, "users:userdb", DEFAULT_USERDB);
		config.uid_db		= iniparser_getstring	(ini, "users:uiddb", DEFAULT_UIDDB);
		config.default_gid	= iniparser_getint	(ini, "users:defaultgid", DEFAULT_GID);
		config.default_perms	= iniparser_getint	(ini, "users:defaultperms", DEFAULT_PERMS);
		
	}
	if(server_start(config.port, config.max_cons, config.dmon, conhandle))
		ret = 0;
	iniparser_freedict(ini);
	return ret;
}

void conhandle()
{
	char *buf;
	int done = 0;
	initialize_shell();
	write_line(config.motd);
	telnet_abilities();
	while(!done) {
		while((!user.exist) && (!done)) {
			if(shell_login()) {
				done = 1;
				kill_connection = 1;
				continue;
			}
		}
		switch(kill_connection) {
			case 1:
				write_line("\nServer is going down.\n");
				done = 1;
				break;
			case 2:
				write_line("\nCtrl-C\n");
				telnet_log("Ctrl-C\n");
				done = 1;
				break;
			case 3:
				done = 1;
				break;
		}
		if(done == 1)
			break;
		write_line("> ");
		buf = read_line();
		write_line("\n");
		if(buf == NULL) {
			telnet_log("Stream cut short.\n");
			done = 1;
		}
		if(!done) {
			error_level = shell(buf);
			free(buf);
		}
	}
	shell_logout();
	destroy_shell();
	write_line("Thank you for visiting SquidNET!\n");
}
