/*****************************************************************************
 *  TrapNET Server                                                           *
 *  Copyright (C)2009-2010    trap15 (Alex Marshall) <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2                                            *
 *****************************************************************************/

#ifndef CONNECTION_H_
#define CONNECTION_H_

typedef void (connection_handler_t)();
extern int kill_connection;
extern int *kill_serv;
extern int sock;
extern int stop_echo;
extern int include_crlf;
extern struct sockaddr_in sock_a;

int server_start(u16 port, int max_connections, int dmon, connection_handler_t conhandle);
void kill_server();
void kill_sock();
ssize_t write_data(const char *buf, ssize_t size);
ssize_t write_line(const char *fmt, ...);
char* read_line();

#endif /* CONNECTION_H_ */
