/*****************************************************************************
 *  TrapNET Server                                                           *
 *  Copyright (C)2009-2010    trap15 (Alex Marshall) <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2                                            *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include "types.h"

#include "iniparser/iniparser.h"
#include "connection.h"
#include "telnet.h"
#include "shell.h"

#define SHELL_CMD_NAME(name)		shell_cmd_##name
#define SHELL_CMD_DEF(name)		int SHELL_CMD_NAME(name)(int argc, char *argv[])
#define SHELL_CMD_EXEC(name, c, v)	SHELL_CMD_NAME(name)(c, v)

#define DO_PERMISSIONS(flag)						\
	if(!(user.perms & (flag))) {					\
		write_line("Permission denied.\n");			\
		telnet_log("Permission denied in %s\n", __FUNCTION__);	\
		return 1;						\
	}

typedef int (exec_t)(int argc, char *argv[]);

typedef struct {
	char	name[64];
	exec_t*	exec;
} shell_cmd_t;

int error_level;
dictionary* userdb;
user_t user;
char cwd[256][256];
int cwddepth = 0;
char basedir[256];

SHELL_CMD_DEF(quit);
SHELL_CMD_DEF(logout);
SHELL_CMD_DEF(shutdown);
SHELL_CMD_DEF(pwd);
SHELL_CMD_DEF(chdir);

#define SHELL_CMD_CNT			5

shell_cmd_t shell_cmds[SHELL_CMD_CNT] = {			\
	{ "quit",	SHELL_CMD_NAME(quit)		},	\
	{ "logout",	SHELL_CMD_NAME(logout)		},	\
	{ "shutdown",	SHELL_CMD_NAME(shutdown)	},	\
	{ "pwd",	SHELL_CMD_NAME(pwd)		},	\
	{ "cd",		SHELL_CMD_NAME(chdir)		},	\
};

SHELL_CMD_DEF(chdir)
{
	char *pch;
	char rpath[256];
	char rpathc[256];
	int i;
	int ret = 0;
	if(argc == 1) {
		if(chdir(basedir) == -1) {
			ret = 1;
			goto end_chdir;
		}
		for(i = 0; i < cwddepth; i++) {
			bzero(cwd[i], 256);
		}
		cwddepth = 0;
		sprintf(cwd[cwddepth++], "");
	}else{
		pch = strtok(argv[1], "/");
		while(pch != NULL) {
			if(memcmp(argv[1], ".", 2) == 0) {
				;
			}else if(memcmp(argv[1], "..", 3) == 0) {
				if(cwddepth > 1) {
					if(chdir("..") == -1) {
						ret = 1;
						goto end_chdir;
					}
					bzero(cwd[--cwddepth], 256);
				}
			}else{
				getcwd(rpathc, 256);
				sprintf(rpath, "\"%s/%s\"", rpathc, pch);
				if(chdir(rpath) == -1) {
					ret = 1;
					telnet_log(rpath);
					printf("\n");
					getcwd(rpath, 256);
					telnet_log(rpath);
					printf("\n");
					goto end_chdir;
				}
				sprintf(cwd[cwddepth++], pch);
			}
			pch = strtok(NULL, "/");
		}
	}
end_chdir:
	if(ret == 1) {
		switch(errno) {
			case ENOTDIR:
				write_line("Not a directory\n");
				break;
			case ENAMETOOLONG:
				write_line("File name too long\n");
				break;
			case ENOENT:
				write_line("No such file or directory\n");
				break;
			case ELOOP:
				write_line("Too redundant\n");
				break;
			case EACCES:
				write_line("Permission denied.\n");
				break;
			case EIO:
				write_line("I/O error\n");
				break;
		}
	}
	return ret;
}

SHELL_CMD_DEF(pwd)
{
	int i;
	for(i = 0; i < cwddepth; i++)
		write_line("%s/", cwd[i]);
	write_line("\n");
	return 0;
}

SHELL_CMD_DEF(quit)
{
	kill_connection = 1;
	return 0;
}

SHELL_CMD_DEF(logout)
{
	shell_logout();
	return 0;
}

SHELL_CMD_DEF(shutdown)
{
	DO_PERMISSIONS(PERM_SHUTDOWN);
	kill_sock();
	kill_server();
	return 0;
}

static char* remove_whitespace(char *buf)
{
	char *rbuf = NULL;
	int i;
	int olen = 0;
	int len;
	int had_white = 0;
	int ignore_space = 0;
	len = strlen(buf);
	for(i = 0; i < len; i++) {
		rbuf = realloc(rbuf, olen + 1);
		if(buf[i] == '\t') {
			if(!had_white)
				rbuf[olen++] = ' ';
			had_white = 1;
		}else if(buf[i] == ' ') {
			if(ignore_space) {
				rbuf[olen++] = ' ';
				ignore_space = 0;
			}else{
				if(!had_white)
					rbuf[olen++] = ' ';
				had_white = 1;
			}
		}else if(buf[i] == '\\') {
			rbuf[olen++] = buf[i];
			ignore_space ^= 1;
		}else{
			rbuf[olen++] = buf[i];
			had_white = 0;
			ignore_space = 0;
		}
	}
	rbuf = realloc(rbuf, olen + 1);
	rbuf[olen] = 0;
	return rbuf;
}

static char** explode_line(char *buf, int *argc)
{
	int i;
	int argadj = 1;
	char **argv = NULL;
	u8 *rbuf;
	int len;
	int ignore_space = 0;
	int curptr = 0;
	rbuf = (u8*)remove_whitespace(buf);
	len = strlen((char*)rbuf);
	*argc = 0;
	for(i = 0; i < len; i++) {
		if(argadj) {
			argv = realloc(argv, ((*argc) + 1) * sizeof(char*));
			argv[*argc] = NULL;
			argadj = 0;
		}
		if(rbuf[i] < 0x20)
			continue;
		if(rbuf[i] >= 0x80)
			continue;
		switch(rbuf[i]) {
			case '\n':
				break;
			case ' ':
				if(!ignore_space) {
					argv[*argc] = realloc(argv[*argc], curptr + 1);
					argv[*argc][curptr] = 0;
					*argc = (*argc) + 1;
					argadj = 1;
					curptr = 0;
					break;
				}
				ignore_space = 0;
				break;
			case '\\':
				ignore_space ^= 1;
				if(ignore_space == 1)
					break;
			default:
				argv[*argc] = realloc(argv[*argc], curptr + 1);
				argv[*argc][curptr++] = rbuf[i];
				ignore_space = 0;
				break;
		}
	}
	*argc = (*argc) + 1;
	return argv;
}

static int compare_cmd(const char *cmd, const char *check)
{
	int i;
	int len = strlen(cmd);
	int len2 = strlen(check) - 1;
	for(i = 0; i < len; i++) {
		if(i > len2)
			break;
		if(cmd[i] != check[i]) {
			return 0;
		}
	}
	return 1;
}

int shell(char *buf)
{
	char** argv;
	int argc;
	int ret = 0;
	int i;
	int found = 0;
	argv = explode_line(buf, &argc);
	for(i = 0; i < SHELL_CMD_CNT; i++) {
		if(compare_cmd(argv[0], shell_cmds[i].name)) {
			ret = shell_cmds[i].exec(argc, argv);
			found = 1;
			break;
		}
	}
	for(i = 0; i < argc; i++)
		free(argv[i]);
	free(argv);
	if(!found) {
		write_line("%s is not a valid command.\n", argv[0]);
		write_line("The following commands are valid:\n");
		for(i = 0; i < SHELL_CMD_CNT; i++) {
			write_line("\t%s\n", shell_cmds[i].name);
		}
		ret = 1;
	}
	return ret;
}

u64 checksum(void *rbuf, int len)
{
	u8* buf8;
	u16* buf16;
	int loc = 0;
	u64 ret = 0;
	u64 base, base1, base2, arg1, arg2;
	buf8  = (u8*) rbuf;
	buf16 = (u16*)rbuf;
	len -= 3;
	while(loc < len) {
		base1  = buf8[loc] & 0xDE;
		base2  = buf8[loc + 1] & 0x7C;
		arg1   = buf8[loc + 2] & 0x75;
		arg2   = buf8[loc + 3] & 0xAB;
		base1 ^= arg1;
		base2 ^= arg2;
		base   = (base1 << 8) | (base2);
		arg1  ^= 0xE3;
		arg1  += arg2;
		base <<= arg1 & 0xF;
		ret   += base;
		loc   += 1;
	}
	return ret;
}

u64 encrypt_pass(char *pass)
{
	return checksum(pass, strlen(pass));
}

static int validate_username(char *usr)
{
	user_t xuser;
	char* node;
	if(strncmp(usr, "new", 3) == 0) {
		return 1;
	}
	asprintf(&node, "%s:exist", usr);
	xuser.exist = iniparser_getboolean(userdb, node, 0);
	free(node);
	if(!xuser.exist)
		return 0;
	return 1;
}

static int validate_password(char *usr, char *pass)
{
	user_t xuser;
	char* node;
	u64 pass_c = 0;
	if((strncmp(usr, "new", 3) == 0) && (strncmp(pass, "user", 4) == 0)) {
		return 1;
	}
	asprintf(&node, "%s:pass", usr);
	xuser.pass = iniparser_getlonglong(userdb, node, 0);
	free(node);
	if(xuser.pass == 0)
		return 0;
	pass_c = encrypt_pass(pass);
	if(pass_c != xuser.pass)
		return 0;
	return 1;
}

int shell_signup()
{
	char *usr = NULL;
	char *pass = NULL;
	char str[64];
	u64 genpass = 0;
	FILE* fp;
	u32 uid = 0;
	telnet_log("Signing up.\n");
	write_line("Please enter desired username and password.\n");
	while(usr == NULL) {
		write_line("User: ");
		READLINE(usr)
		if(validate_username(usr)) {
			write_line("An account with that name already exists.\n");
			telnet_log("User %s already exists.\n", usr);
			free(usr);
			usr = NULL;
			continue;
		}
	}
	while(pass == NULL) {
		write_line("\nPassword: ");
		stop_echo = 1;
		READLINE(pass)
		stop_echo = 0;
		write_line("\n");
		if(strlen(pass) < 4) {
			write_line("Password is too short. Must be at least 4 characters.\n");
			free(pass);
			pass = NULL;
		}
	}
	genpass = encrypt_pass(pass);
	free(pass);
	fp = fopen(config.uid_db, "wb+");
	if(fp) {
		fread(&uid, 4, 1, fp);
		uid++;
		fwrite(&uid, 4, 1, fp);
		fclose(fp);
	}
	iniparser_freedict(userdb);
	fp = fopen(config.user_db, "a");
	bzero(str, 64);
	sprintf(str, "[%s]\n", usr);
	fwrite(str, strlen(str), 1, fp);
	bzero(str, 64);
	sprintf(str, "exist = Yes\n");
	fwrite(str, strlen(str), 1, fp);
	bzero(str, 64);
	sprintf(str, "pass = 0x%016llX\n", genpass);
	fwrite(str, strlen(str), 1, fp);
	bzero(str, 64);
	sprintf(str, "uid = %d\n", uid);
	fwrite(str, strlen(str), 1, fp);
	bzero(str, 64);
	sprintf(str, "gid = %d\n", config.default_gid);
	fwrite(str, strlen(str), 1, fp);
	bzero(str, 64);
	sprintf(str, "perms = 0x%08X\n", config.default_perms);
	fwrite(str, strlen(str), 1, fp);
	bzero(str, 64);
	sprintf(str, "\n");
	fwrite(str, strlen(str), 1, fp);
	fclose(fp);
	mkdir(usr, S_IRWXU | S_IRWXG | S_IRWXO);
	free(usr);
	return 0;
}

static int log_in(char *usr)
{
	char node[64];
	if(strncmp(usr, "new", 3) == 0) {
		if(shell_signup())
			return 2;
		userdb = iniparser_load(config.user_db);
		if(userdb == NULL) {
			telnet_log("Unable to open user database!\n");
			return 0;
		}
		write_line("You are now signed up. Please log in.\n");
		return 0;
	}
	sprintf(node, "%s:exist", usr);
	user.exist = iniparser_getboolean(userdb, node, 0);
	if(!user.exist)
		return 0;
	sprintf(node, "%s:uid", usr);
	user.uid = iniparser_getint(userdb, node, 0xFFFFFFFF);
	sprintf(node, "%s:gid", usr);
	user.gid = iniparser_getint(userdb, node, 0xFFFFFFFF);
	sprintf(node, "%s:perms", usr);
	user.perms = iniparser_getint(userdb, node, 0);
	chdir(usr);
	sprintf(cwd[cwddepth++], "");
	getcwd(basedir, 256);
	return 1;
}

int shell_login()
{
	int logged_in = 0;
	char *usr = NULL;
	char *pass = NULL;
	int ret;
	int gret = 0;
	int failedcnt = 0;
	telnet_log("Logging in...\n");
	userdb = iniparser_load(config.user_db);
	if(userdb == NULL) {
		telnet_log("Unable to open user database!\n");
		return 1;
	}
	bzero(&user, sizeof(user_t));
	while(!logged_in) {
		if(failedcnt >= config.max_logins) {
			write_line("Too many failed logins. Goodbye.\n");
			telnet_log("Too many failed logins.\n");
			gret = 1;
			break;
		}
		if(usr != NULL) {
			free(usr);
			usr = NULL;
		}
		if(pass != NULL) {
			free(pass);
			pass = NULL;
		}
		write_line("User: ");
		READLINE(usr)
		write_line("\n");
		if(!validate_username(usr)) {
			write_line("No such user %s.\n", usr);
			telnet_log("Failed to login as %s. No such user.\n", usr);
			failedcnt++;
			continue;
		}
		write_line("Password: ");
		stop_echo = 1;
		READLINE(pass)
		stop_echo = 0;
		write_line("\n");
		if(!validate_password(usr, pass)) {
			write_line("Invalid password for %s.\n", usr);
			telnet_log("Failed to login as %s. Invalid password %s.\n", usr, pass);
			failedcnt++;
			continue;
		}
		ret = log_in(usr);
		if(ret == 2) {
			gret = 1;
			break;
		}else if(ret) {
			write_line("Welcome to SquidNET, %s!\n", usr);
			logged_in = 1;
		}
	}
	if(usr != NULL) {
		free(usr);
		usr = NULL;
	}
	if(pass != NULL) {
		free(pass);
		pass = NULL;
	}
	return gret;
}	

int shell_logout()
{
	int i;
	bzero(&user, sizeof(user_t));
	iniparser_freedict(userdb);
	for(i = 0; i < cwddepth; i++) {
		chdir("..");
		bzero(cwd[i], 256);
	}
	return 1;
}

int initialize_shell()
{
	error_level = 0;
	return 1;
}

int destroy_shell()
{
	bzero(&user, sizeof(user_t));
	iniparser_freedict(userdb);
	return 1;
}
