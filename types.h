/*****************************************************************************
 *  TrapNET Server                                                           *
 *  Copyright (C)2009-2010    trap15 (Alex Marshall) <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2                                            *
 *****************************************************************************/

#ifndef TYPES_H_
#define TYPES_H_

#include <stdint.h>

typedef uint8_t			u8;
typedef uint16_t		u16;
typedef uint32_t		u32;
typedef uint64_t		u64;

typedef int8_t			s8;
typedef int16_t			s16;
typedef int32_t			s32;
typedef int64_t			s64;

#endif /* TYPES_H_ */
